package org.techpoa.combinatorpattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
public class Customer {

    private final String  name,email, phone;
    private final LocalDate dob;



}
