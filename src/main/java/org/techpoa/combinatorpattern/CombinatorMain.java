package org.techpoa.combinatorpattern;

import java.time.LocalDate;
import static org.techpoa.combinatorpattern.CustomerRegistrationValidator.*;

public class CombinatorMain {

    public static void main(String[] args) {

        Customer customer = new Customer(
                "Alice",
                "alice@example.net",
                "+1982921312312",
                LocalDate.of(1995,6,13)
        );


        //using combinator pattern
        //allows flexible maintenance of functions
        ValidationResult result = isEmailValid()
                .and(isAdult())
                .and(isPhoneValid())
                .apply(customer);

        // this applies lazy loading where not unless we invoke the apply method nothing happens.

        System.out.println(result);

        if (result != ValidationResult.SUCCESS)
        {
            throw new IllegalStateException(result.name());
        }

    }

}
