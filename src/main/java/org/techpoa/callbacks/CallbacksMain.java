package org.techpoa.callbacks;

import java.util.function.Consumer;

public class CallbacksMain {

    public static void main(String[] args) {

        // We can use functional interfaces to do callbacks as implemented in javascript

        hello("Dan",null, value -> {
            System.out.println("No Last name provided for "   + value);
        });

        //in this case we are not taking any value
        helloTwo("Dan",null, () -> {
            System.out.println("No Last name provided");
        });

    }



    static void hello(String firstName, String lastName, Consumer<String> callback)
    {
        System.out.println(firstName);
        if (lastName != null)
        {
            System.out.println(lastName);
        } else {
            callback.accept(firstName);
        }
    }


    static void helloTwo(String firstName, String lastName, Runnable callback)
    {
        System.out.println(firstName);
        if (lastName != null)
        {
            System.out.println(lastName);
        } else {
            callback.run();
        }
    }

}
