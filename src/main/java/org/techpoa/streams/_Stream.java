package org.techpoa.streams;

import java.util.List;
import java.util.stream.Collectors;

import static org.techpoa.streams._Stream.Person.*;
import static org.techpoa.streams._Stream.Gender.*;

public class _Stream {

    public static void main(String[] args) {

        List<_Stream.Person> people = List.of(
                new _Stream.Person("Maria", FEMALE),
                new _Stream.Person("John", MALE),
                new _Stream.Person("AISHA", FEMALE),
                new _Stream.Person("Jonah", MALE),
                new _Stream.Person("Alice", FEMALE)
        );

        people.stream()
                .map(person -> person.gender)
                .collect(Collectors.toSet())
                .forEach(System.out::println);

        boolean containsOnlyFemales = people.stream()
                .allMatch(person -> FEMALE.equals(person.gender));
        System.out.println(containsOnlyFemales);

    }


    static class Person {
        private final String name;
        private final _Stream.Gender gender;

        Person(String name, _Stream.Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    static enum Gender {
        MALE, FEMALE
    }

}
