package org.techpoa.streams.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class Company {
    private String name;
    private Address address;
    private List<Person> personList;
}
