package org.techpoa.streams.dto;

import lombok.Getter;

@Getter
public class Address {
    private String street;
    private City city;
}
