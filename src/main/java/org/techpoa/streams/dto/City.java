package org.techpoa.streams.dto;

import lombok.Getter;

@Getter
public class City {
    private String name;
    private State state;
}
