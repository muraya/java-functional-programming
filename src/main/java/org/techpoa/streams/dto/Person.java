package org.techpoa.streams.dto;

import lombok.Getter;

@Getter
public class Person {
    private Long id;
    private String name;
}
