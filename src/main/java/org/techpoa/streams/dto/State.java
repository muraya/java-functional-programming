package org.techpoa.streams.dto;

import lombok.Getter;

@Getter
public class State {
    private String name;
}
