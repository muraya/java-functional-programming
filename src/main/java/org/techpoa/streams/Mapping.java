package org.techpoa.streams;

import org.techpoa.streams.dto.Address;
import org.techpoa.streams.dto.City;
import org.techpoa.streams.dto.Company;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Mapping {

    public static void main(String[] args) {

    }

    /**
     * Returns List of City names in the company address
     *
     * @param companyList
     * @return
     */

    public List<String> getCityNames(List<Company> companyList) {
        return companyList.stream()
                .map(company -> company.getAddress().getCity().getName())
                .toList();
    }

    public List<String> getCityNamesImproved(List<Company> companyList) {

        return companyList.stream().map(Company::getAddress).map(Address::getCity).map(City::getName).toList();

    }

    public List<String> getCityNamesWithNullChecks(List<Company> companyList) {

        return companyList.stream()
                .map(Company::getAddress)
                .filter(Objects::nonNull)
                .map(Address::getCity).map(City::getName).toList();

    }

}
