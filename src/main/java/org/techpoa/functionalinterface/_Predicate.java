package org.techpoa.functionalinterface;

import java.util.function.Predicate;

public class _Predicate {

    public static void main(String[] args) {

        System.out.println(phoneNumberisValid.test("01021364789"));
        System.out.println(phoneNumberisValid.test("07001247859"));

    }

     static Predicate<String> phoneNumberisValid = phoneNumber -> phoneNumber.startsWith("07") && phoneNumber.length() == 11;
}
