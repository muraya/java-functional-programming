package org.techpoa.functionalinterface;

import java.sql.SQLOutput;
import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {

    public static void main(String[] args) {

        // function example takes one argument and produses one result
        int increment = increment(0);
        System.out.println(increment);

        System.out.println(incrementByOneFunction.apply(1));

        //chaining functions
        System.out.println(incrementByOneFunction.andThen(multiplyByTen).apply(2));

        //BiFunction takes two arguments and return one result
        System.out.println(increaseByOneThenMultiply.apply(1,10));

    }

    // Takes one argument then return another argument
    static int increment(int num)
    {
        return  num + 1;
    }


    //Using functional interface
    // function takes one argument with the second argument as the response
    // Function<Input Type, Return type>

   static Function<Integer, Integer> incrementByOneFunction = number -> number ++;

   static Function<Integer, Integer> multiplyByTen = number -> number  * 10;

   //BiFunctions
    // functions taking two inputs then outputting one output.
    // Function<Integer, Integer , Integer>

    static BiFunction<Integer, Integer,Integer> increaseByOneThenMultiply =
            (numberToIncrementByOne, numberToMultiply ) -> (numberToIncrementByOne + 1) * numberToMultiply;


    //Connsumer
    // Operation that accepts single input then does not return any result
    // just like a void function in normal programming construct

}
