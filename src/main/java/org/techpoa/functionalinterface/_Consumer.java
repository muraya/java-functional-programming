package org.techpoa.functionalinterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {

    public static void main(String[] args) {
        // Using consumer functional interface
        greetCustomer.accept(new Customer("Maria", "999999"));

        greetCustomerBiFunction.accept(new Customer("Dan", "64198482"), false);
    }



    //Consumer
    //accepts one input and gives no result
    // just like void function in programming
    // customer being the datatype of the inout as expected

    static Consumer<Customer> greetCustomer = customer -> System.out.println("Hello " + customer.name + " thanks or registering with " + customer.phone);

    // accepts two arguments
    static BiConsumer<Customer, Boolean>   greetCustomerBiFunction =  ((customer, showPhoneNumber) -> System.out.println(
            "Hello " + customer.name + " thanks or registering :: " + (showPhoneNumber ? customer.phone : "+++++++++++")
    ));

    static class Customer {
        private final String name;
        private  final String phone;

        Customer(String name, String phone)
        {
            this.name = name;
            this.phone = phone;
        }

    }

}
