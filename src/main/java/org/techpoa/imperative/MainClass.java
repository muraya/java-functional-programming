package org.techpoa.imperative;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.techpoa.imperative.MainClass.Gender.*;
import static org.techpoa.imperative.MainClass.Person.*;


public class MainClass {

    public static void main(String[] args) {

        List<Person> people;
        people = List.of(
                new Person("John", MALE),
                new Person("Maria", FEMALE),
                new Person("AISHA", FEMALE),
                new Person("Jonah", MALE),
                new Person("Alice", FEMALE)
        );

        // Imperative approach to programming
        //print out the females in the list

        List<Person> females = new ArrayList<>();

        for (Person person: people)
        {
            if (FEMALE.equals(person.gender))
            {
                females.add(person);
            }
        }

        for (Person female : females) {
            System.out.println(female);
        }


        // Declarative approach

        people.stream().filter(person -> FEMALE.equals(person.gender))
                .collect(Collectors.toList())
                .forEach(System.out::println);




    }

    public static class Person {
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    public static enum Gender {
        MALE, FEMALE
    }


}
